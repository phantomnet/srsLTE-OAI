#!/usr/bin/env python

#
# Standard geni-lib/portal libraries
#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as elab
import geni.rspec.igext as IG
import geni.urn as URN


tourDescription = """
Use this profile to instantiate an experiment using srsLTE UE or Nexus 5 as UE, srsLTE eNB and Open Air Interface Core Network
to realize an end-to-end SDR-based mobile network. 
""";

tourInstructions = """
Please refer to /local/repository/config/README in sdrue, sdrenb, or epc node
""";


#
# PhantomNet extensions.
#
import geni.rspec.emulab.pnext as PN

#
# Globals
#
class GLOBALS(object):
    OAI_DS = "urn:publicid:IDN+emulab.net:phantomnet+ltdataset+oai-develop"
    OAI_SIM_DS = "urn:publicid:IDN+emulab.net:phantomnet+dataset+PhantomNet:oai"
#    SRSLTE="urn:publicid:IDN+emulab.net+image+PhantomNet:openlte-0-20-4Scan"
    SRSLTE="urn:publicid:IDN+emulab.net+image+PhantomNet:srsLTE"
    UE_IMG  = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ANDROID444-STD")
    #urn:publicid:IDN+emulab.net+image+PhantomNet:srsEPC-OAICN
    OAI_EPC_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:srsEPC-OAICN")
    ADB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU14-64-PNTOOLS")
    UE_HWTYPE = "nexus5"
    OAI_CONF_SCRIPT = "/usr/bin/sudo /local/repository/bin/config_oai.pl"

def connectOAI_DS(node, sim):
    # Create remote read-write clone dataset object bound to OAI dataset
    bs = request.RemoteBlockstore("ds-%s" % node.name, "/opt/oai")
    if sim == 1:
	bs.dataset = GLOBALS.OAI_SIM_DS
    else:
	bs.dataset = GLOBALS.OAI_DS
    bs.rwclone = True

    # Create link from node to OAI dataset rw clone
    node_if = node.addInterface("dsif_%s" % node.name)
    bslink = request.Link("dslink_%s" % node.name)
    bslink.addInterface(node_if)
    bslink.addInterface(bs.interface)
    bslink.vlan_tagging = True
    bslink.best_effort = True

#
# This geni-lib script is designed to run in the PhantomNet Portal.
#
pc = portal.Context()

#
# Profile parameters.
#
#pc.defineParameter("RADIATEDRF", "Radiated (over-the-air) RF transmissions",
#                   portal.ParameterType.BOOLEAN, False,
#                   longDescription="When enabled, RF devices with real antennas and transmissions propagated through free space will be selected.  Leave disabled (default) to assign RF devices connected via transmission lines with variable attenuator control.")

pc.defineParameter("TYPE", "UE type",
                   portal.ParameterType.STRING,"nexus5",[("srsue","SRS UE"),("nexus5","Nexus 5")],
                   longDescription="Type of UEs connected to eNB")

params = pc.bindParameters()

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = pc.makeRequestRSpec()
epclink = request.Link("s1-lan")


# add sdr enb
sdrenb = request.RawPC( "sdrenb" )
sdrenb.hardware_type = "nuc5300"
sdrenb.disk_image = GLOBALS.SRSLTE
sdrenbif1 = sdrenb.addInterface( "renb1" )
epclink.addNode(sdrenb)



# add sdr ue
if params.TYPE == "srsue":
    ue = request.RawPC( "sdrue" )
    ue.hardware_type = "nuc5300"
    ue.disk_image = GLOBALS.SRSLTE
    ueif1 = ue.addInterface( "ue1" )
    #sdrueif2 = sdrue.addInterface( "rue2" )
else:
    adb_t = request.RawPC("adb-tgt")
    adb_t.disk_image = GLOBALS.ADB_IMG
    ue = request.UE("rue1")
    ue.hardware_type = GLOBALS.UE_HWTYPE
    ue.disk_image = GLOBALS.UE_IMG
    ue.adb_target = "adb-tgt"
    ueif1 = ue.addInterface( "ue1" )

# Create the RF link between the srsUE, srseNB and eNodeB
rflink1 = request.RFLink("rflink1")
rflink1.addInterface(sdrenbif1)
rflink1.addInterface(ueif1)


# Add OAI EPC (HSS, MME, SPGW) node.
epc = request.RawPC("epc")
epc.disk_image = GLOBALS.OAI_EPC_IMG
epc.addService(rspec.Execute(shell="sh", command=GLOBALS.OAI_CONF_SCRIPT + " -r EPC"))
connectOAI_DS(epc, 0)
 
epclink.addNode(epc)
epclink.link_multiplexing = True
epclink.vlan_tagging = True
epclink.best_effort = True

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

#
# Print and go!
#
pc.printRequestRSpec(request)
