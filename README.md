# About This Profile

Be sure to setup your SSH keys as outlined in the manual; it's better
to log in via a real SSH client to the nodes in your experiment.

The Open Air Interface source is located under `/opt/oai` on epc nodes.  
It is mounted as a clone of a remote blockstore
(dataset) maintained by PhantomNet.  Feel free to change anything in
here, but be aware that your changes will not persist when your
experiment terminates.

This experiment can work in two modes:
1. Nexus 5 UE 
2. SRS UE using SDR

You can select one of them, in the drop down menu "UE type",
while instantiating the experiment (i.e., in Parameterize step)

# Getting Started with Nexus 5 UE

############################ Run OAI-CN ############################
1. Run OAI-CN

$ sudo /local/repository/bin/start_oai.pl

This will stop any currently running OAI services, start all services
(mme. sgw, and pge) again.
It saves logs to `/var/log/oai/*` on the and `epc` nodes.

2. Check whether mme, hss, spgw run or not.  

$ sudo screen -list  
There are screens on:  
	16227.spgw	(06/12/2018 12:05:25 PM)	(Detached)  
	16130.mme	(06/12/2018 12:05:19 PM)	(Detached)  
	16053.hss	(06/12/2018 12:05:13 PM)	(Detached)  
3 Sockets in /var/run/screen/S-root.  


Note that ignore below message if you can see above output after running start_oai.pl  

"ssh: Could not resolve hostname enb1: Name or service not known  
ERROR: Could not detect USRP B210 radio on the enb1 node. This is usually a transient error. Reboot the enb1 node and try again."  



############################ Run srsLTE eNB ############################

1. Make cpu performance  
$ cd /local/repository/config/srsLTE  
$ sudo bash governor.sh  

2. Change enb configuration file (e.g., change rx_gain values. I tested 30 or 40).   
$ cd /usr/local/src/srsLTE/build/srsenb/src  
$ vim enb.conf  
tx_gain = 90  
rx_gain = 30  

3. Run srsLTE eNB  
$ cd /usr/local/src/srsLTE/build/srsenb/src  
$ sudo ./srsenb ./enb.conf  

############################ Run Nexus 5 UE ############################  
$ pnadb -a  
$ adb shell  
$ ping 8.8.8.8  

When using real UE, to access the UE via ADB, first log in to the `adb-tgt`
node, then run `pnadb -a` to connect to the UE.  Use ADB commands as per
normal afterward.  If/when you reboot the UE, be aware that you will need
to again run `pnadb -a` to reestablish the ADB connection; wait a minute
or so for the UE to become available again before doing this.


# Getting Started with srs UE

###################### Default configurations of UE subscription and Radio ###############
1. User subscription info  
imsi : 998981234560308  
algo : milenage  
op   : 01020304050607080910111213141516  
k    : 00112233445566778899aabbccddeeff  
amf  : 8000  

2. LTE band  
LTE band : 4  

############################ Run OAI-CN ############################
1. Provision UE in OAI-CN HSS  
$ cd  /local/repository/config/oai-db  
$ mysql -u root -p # password : linux  
mysql> use oai_db  
`Run mysql commands from users.sh and pdn.sh files`

2. Run OAI-CN  
$ sudo /local/repository/bin/start_oai.pl  


3. Check whether mme, hss, spgw run or not.  

$ sudo screen -list  
There are screens on:  
	16227.spgw	(06/12/2018 12:05:25 PM)	(Detached)  
	16130.mme	(06/12/2018 12:05:19 PM)	(Detached)  
	16053.hss	(06/12/2018 12:05:13 PM)	(Detached)  
3 Sockets in /var/run/screen/S-root.  


Note that ignore below message if you can see above output after running start_oai.pl  

"ssh: Could not resolve hostname enb1: Name or service not known  
ERROR: Could not detect USRP B210 radio on the enb1 node. This is usually a transient error. Reboot the enb1 node and try again."  


############################ Run srsLTE eNB ############################  
1. Make cpu performance  
$ cd /local/repository/config/srsLTE  
$ sudo bash governor.sh  

2. Run srsLTE eNB  
$ cd /usr/local/src/srsLTE/build/srsenb/src  
$ sudo ./srsenb ./enb.conf  



############################ Run srsLTE UE ############################  
1. Make cpu performance  
$ cd /local/repository/config/srsLTE  
$ sudo bash governor.sh  

2. Run srsLTE UE  
$ cd /usr/local/src/srsLTE/build/srsue/src  
$ sudo ./srsue ./ue.conf  

`If you see below message, ue is correctly connected and this is ip address assigned for the ue.`  
Network attach successful. IP: 172.16.0.6  


3. Sending traffic  
# It needs to set up routing tables to use LTE/EPC connectivity.  
# change mtu size - mandatory??  
$ sudo ip link set mtu 1400 dev tun_srsue  
# Set up routing table  
# You need to use right ip address for gw as your tun_srsue ip address.  
# In this example, ue gets 172.16.0.6 ip address from EPC  
$ sudo route add -net 8.8.8.8 netmask 255.255.255.255 gw 172.16.0.6 dev tun_srsue  
$ ping 8.8.8.8

`You can use a script in /local/repository/config/srsLTE/srsue/ping-to-google.sh for above commands.`  
$ sudo bash ping-to-google.sh 172.16.0.6  


############################ Run srsLTE EPC ############################  
Currently, it configures user information which is the same as srsLTE UE.
If you use Nexus 5, you need to change user_db.csv file in /usr/local/src/srsLTE/build/srsepc/src directory.
You need to change the second column (i.e., IMSI information). Refer to below descriptions.      

1. Set up routing path for UE traffic. Note that you need to specify a network interface which has public IP address (e.g., 155.98.x.x) in your experiment. In general, its name is `eno1` in emulab experiments. But you need to check it with `ifconfig`.   
$ cd /usr/local/src/srsLTE/srsepc  
$ sudo ./if_masq.sh <Interface Name>

2. Run srsLTE EPC   
$ cd /usr/local/src/srsLTE/build/srsepc/src   
$ sudo ./srsepc ./epc.conf




############################ When run Nexus 5 and srsLTE EPC ############################  
If you use Nexus 5, you need to change `user_db.csv` file in /usr/local/src/srsLTE/build/srsepc/src directory.
To know IMSI of Nexus 5, please use `N5_SIM.sh` and use the output in `user_db.csv`.      

1. Access to `adb-tgt` node.   
$ cd /local/repository/config/srsLTE/srsepc  
$ bash ./N5_SIM.sh


For more detailed information:

  * [Controlling OAI](https://gitlab.flux.utah.edu/powder-profiles/OAI-Real-Hardware/blob/master/control.md)
  * [Inspecting OAI](https://gitlab.flux.utah.edu/powder-profiles/OAI-Real-Hardware/blob/master/inspect.md)
  * [Modifying OAI](https://gitlab.flux.utah.edu/powder-profiles/OAI-Real-Hardware/blob/master/modify.md)
  * [Modifying This Profile](https://gitlab.flux.utah.edu/powder-profiles/OAI-Real-Hardware/blob/master/modify-profile.md)
